let http = require('http')
let post = 4000

const server=http.createServer((req, res)=>{
    if(req.url == '/' && req.method == 'GET'){
        res.writeHead(200,{'Content-Type':'text/plain'});
        res.end('Welcome to Booking System')
    }
    if(req.url=='/profile' && req.method=='GET'){
        res.writeHead(200,{'Content-Type':'text/plain'});
        res.end("Welcome to your profile!");
    }
    if(req.url=='/courses' && req.method=='GET'){
        res.writeHead(200,{'Content-Type':'text/plain'});
        res.end("Here's our courses available");
    }
    if(req.url == '/addcourse' && req.method == 'POST'){
        res.writeHead(200,{'Content-Type':'text/plain'});
        res.end('Add a course to our to our resources')
    }
}).listen(4000)

console.log(`Server running at localhost: ${post}`)